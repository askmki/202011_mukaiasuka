package jp.alhinc.springtraining.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class SettingUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public SettingUserForm getSettingUser(Long id) {	
		User entity = mapper.editUser(id);
		SettingUserForm form = new SettingUserForm();
		BeanUtils.copyProperties(entity, form);
		
		return form;
	}
	
	@Transactional
	public int updateUser(SettingUserForm form) {
		User entity = new User();
		BeanUtils.copyProperties(form, entity);
		entity.setName(form.getName());
		entity.setLoginId(form.getLoginId());
		entity.setBranchId(form.getBranchId());
		entity.setPositionId(form.getPositionId());

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getPassword()));

		return mapper.setUser(entity);
	}
	
	public int existingUser(String loginId) {
		return mapper.existingUser(loginId);
	}
}
