package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.DeleteUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class DeleteUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int delete(DeleteUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		entity.setIs_delete(form.getIs_delete());
		
		return mapper.delete(entity);
	}
}
