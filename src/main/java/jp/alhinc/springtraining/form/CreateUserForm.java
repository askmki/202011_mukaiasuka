package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
//import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;



public class CreateUserForm implements Serializable {
	
	@NotBlank(message="必須項目です")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message="半角英数字のみ有効です")
	public String loginId;

	@NotBlank(message="名前を入力してください")
	@Size(max=10,message="10文字以下で入力して下さい")
	public String name;

	@NotBlank(message="PWを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}",message="半角英数字のみ有効です")
	public String rawPassword;
	
	@NotBlank(message="確認用パスワードを入力してください")
	public String secondPassword;
	@AssertTrue(message="エラーです")
	public boolean isPasswordValid() {
		if(rawPassword.equals(secondPassword))
		return true;
		return false;
	}

	public int branch;

	public int position;
	
	public Long id;
	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch(int branch) {
		this.branch = branch;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}

}
