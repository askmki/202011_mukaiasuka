package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SettingUserForm implements Serializable {


	@NotBlank(message="必須項目です")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message="半角英数字のみ有効です")
	public String loginId;

	@NotBlank(message="名前を入力してください")
	@Size(max=10,message="10文字以下で入力して下さい")
	public String name;

	@NotBlank(message="PWを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}",message="半角英数字のみ有効です")
	
	public String password;
	
	@NotBlank(message="確認用パスワードを入力してください")
	public String secondPassword;
	@AssertTrue(message="エラーです")
	public boolean isPasswordValid() {
		if(password.equals(secondPassword))
		return true;
		return false;
	}
	
	private int branchId;

	private int positionId;
	
	private Long id;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String Password) {
		this.password = Password;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSecondPassword() {
		return secondPassword;
	}
	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}
}
