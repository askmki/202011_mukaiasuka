package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingUserForm;

@Mapper
public interface UserMapper {

	List<User> findAll();
	List<Branch> mapperBranch();
	List<Position> mapperPosition();
	
	int create(User entity);
	
	User editUser(Long id);
	int setUser(User entity);
	
	int delete(User entity);
	
	int existingUser(String loginId);
}
