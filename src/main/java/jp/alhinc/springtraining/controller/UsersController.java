package jp.alhinc.springtraining.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
//import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.DeleteUserForm;
import jp.alhinc.springtraining.form.SettingUserForm;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.DeleteUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.SettingUserService;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private SettingUserService settingUserService;

	@Autowired
	private DeleteUserService deleteUserService;

	public static final String STR_BRANCHES = "branch";
	public static final String STR_POSITIONS = "position";
	
	@GetMapping
	public String index(Model model) {
		List<User> users = getAllUsersService.getAllUsers(); //ユーザー情報
		model.addAttribute("users", users);

		return "users/index";
	}

	@GetMapping("/create")
	public String create(Model model) {
		List<Branch> branches = getAllUsersService.getBranches();  //支店情報
		List<Position> positions = getAllUsersService.getPositions();  //役職情報
		model.addAttribute(STR_BRANCHES,branches);
		model.addAttribute(STR_POSITIONS,positions);
		model.addAttribute("form", new CreateUserForm());
		return "users/create";
	}

	@PostMapping
	public String create(@ModelAttribute("form") @Valid CreateUserForm form, BindingResult result, Model model) {
		List<Branch> branches = getAllUsersService.getBranches();
		List<Position> positions = getAllUsersService.getPositions();
//		if (result.hasErrors()) {
//			model.addAttribute(STR_BRANCHES,branches);
//			model.addAttribute(STR_POSITIONS,positions);
//			return "users/create";
//		}
		
		int loginId = createUserService.existingUser(form.getLoginId());
		if (loginId == 0 ) {
			//重複していなかった場合登録に進む
			createUserService.create(form);
			return "redirect:/users";
		} else {
			model.addAttribute(STR_BRANCHES,branches);
			model.addAttribute(STR_POSITIONS,positions);
			model.addAttribute("message","ログインIDが重複しています");
			return "users/create";
		}
	}

	@GetMapping("/setting")    //編集画面
	public String setting(Model model ,@RequestParam Long id) {
		SettingUserForm form = settingUserService.getSettingUser(id);
		List<Branch> branches = getAllUsersService.getBranches();
		List<Position> positions = getAllUsersService.getPositions();
		model.addAttribute(STR_BRANCHES,branches);
		model.addAttribute(STR_POSITIONS,positions);	
		model.addAttribute("form", form);
		return "users/setting";
	}

	@PostMapping("/setting")
	public String setting(@ModelAttribute("form") @Valid SettingUserForm form, BindingResult result, Model model) {

		List<Branch> branches = getAllUsersService.getBranches();
		List<Position> positions = getAllUsersService.getPositions();
		if (result.hasErrors()) {
			model.addAttribute(STR_BRANCHES,branches);
			model.addAttribute(STR_POSITIONS,positions);
			return "users/setting";
		}
		
		int loginId = settingUserService.existingUser(form.getLoginId());
		if (loginId == 0 ) {
			//重複していなかった場合編集処理に進む
		settingUserService.updateUser(form);
		return "redirect:/users";
		} else {
			model.addAttribute(STR_BRANCHES,branches);
			model.addAttribute(STR_POSITIONS,positions);
			model.addAttribute("message","ログインIDが重複しています");
			return "users/setting";
		}
	}

	@PostMapping("/stop")      //ユーザ停止.復活
	public String delete(DeleteUserForm form, Model model) {
		deleteUserService.delete(form);
		return "redirect:/users";
	}
}